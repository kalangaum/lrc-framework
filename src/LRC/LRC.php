<?php

namespace LRC;

use LRC\db\DBConnect;
use LRC\util\ServerUtil;
use LRC\util\LRCConstants as C;

/**
 * Main class to configure the framework
 */
final class LRC
{
    public static function sessionStart()
    {
        if(session_status() != PHP_SESSION_ACTIVE)
        {
            session_start();
        }
    }
    
    /**
     * Registers a DB configuration based on its host
     * 
     * @param String $http_host host url
     * @param String $className classname for the config
     */
    public static function linkDBConnectionToEnvironment($http_host, $className)
    {
        $GLOBALS[$http_host] = $className;
    }
    
    /**
     * Define o src de toggles e qual o tipo de conexão requerida para ler os dados
     * @param $connector
     * @param $src
     * @param host
     */
    public static function setToggleConfig($connector, $src, $host)
    {
        
        if(!isset($GLOBALS[C::LRC_TOGGLES]))
        {
            $GLOBALS[C::LRC_TOGGLES] = [];
        }
        
        $GLOBALS[C::LRC_TOGGLES][$host] = [];
        $GLOBALS[C::LRC_TOGGLES][$host][C::LRC_TOGGLE_CONNECTOR] = $connector;
        $GLOBALS[C::LRC_TOGGLES][$host][C::LRC_TOGGLE_SOURCE] = $src;
        
    }

    /**
     * Initialize the framework
     */
    public static function init()
    {
        DBConnect::init();
        \Flight::start();
    }
    
}
