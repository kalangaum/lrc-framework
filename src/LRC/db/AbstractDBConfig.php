<?php

namespace LRC\db;

/**
 * Abstract class that need to be configured for each environment
 */
abstract class AbstractDBConfig
{
    /*
     * @return the db host
     */
    abstract public function getDBHost();
    
     /*
     * @return the db name
     */
    abstract public function getDBName();
    
     /*
     * @return the db username
     */
    abstract public function getUsername();
    
     /*
     * @return the db password
     */
    abstract public function getPassword();
    
}
