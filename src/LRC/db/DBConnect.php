<?php

namespace LRC\db;

use ORM;
use PDO;
use LRC\util\ServerUtil;

/**
 * Instanciates and configures a database connection
 */
class DBConnect {
    
    /**
     * Iniciate the config process.
     */
    public static function init()
    {
        
        $configClass = $GLOBALS[ServerUtil::getHostUrl()];
        $dbconf = new $configClass();
        self::iniciaDB($dbconf);
    }
    
    /**
     * Configures ORM connection based on received config
     * @param AbstractDBConfig $dbconf - object with connection configs based on environment
     */
    private static function iniciaDB(AbstractDBConfig $dbconf)
    {
        ORM::configure('id_column', 'ID');
        ORM::configure('error_mode', PDO::ERRMODE_EXCEPTION);
        
        ORM::configure('mysql:host='.$dbconf->getDBHost().';dbname='.$dbconf->getDBName());
        ORM::configure('username', $dbconf->getUsername());
        ORM::configure('password', $dbconf->getPassword());
    }
    
}
