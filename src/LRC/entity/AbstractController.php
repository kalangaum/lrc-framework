<?php

namespace LRC\entity;

use Exception;

/*
 * Abstract class for controllers
 */
abstract class AbstractController
{
    
    protected $request;
    
    public function __construct() {
        $this->request = \Flight::request();
    }
    
//    protected static function modelName()
//    {
//        throw new Exception('Método modelName não implementado em '.static::class);
//    }
//    
//    protected static function serviceName()
//    {
//        throw new Exception('Método serviceName não implementado em '.static::class);
//    }
    
}