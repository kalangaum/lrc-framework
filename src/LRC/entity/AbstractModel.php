<?php

namespace LRC\entity;

use Exception;

/*
 * Abstract class for models
 */
abstract class AbstractModel
{
    
    public $id = null;
    
    /**
     * Instanciates a new model
     */
    public function __construct() {
        $this->id = 0;
    }
    
    /**
     * Saves the object
     */
    public final function salvar()
    {
        $this->beforeSave();
        $this->save();
        $this->afterSave();
    }
    
    /**
     * method called before the item is saved
     */
    protected function beforeSave()
    {
        
    }
    
    /**
     * calls service to save the object
     */
    protected function save()
    {
        $serviceName = static::serviceName();
        $id = $serviceName::save($this);
        $this->id = $id;
    }
    
    /**
     * method called after the object is saved
     */
    protected function afterSave()
    {
        
    }
    
    protected static function serviceName()
    {
        throw new Exception('Método serviceName não implementado em '.static::class);
    }
    
}
