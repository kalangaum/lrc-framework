<?php

namespace LRC\entity;

use ORM;
use LRC\util\ORMUtil;
use LRC\entity\AbstractModel;
use Exception;

/*
 * Abstract class for services
 */
abstract class AbstractService
{

    /**
     * Deletes an object by its ID
     * 
     * @param int $id - id for the object to be deleted
     * @return bool if it was deleted
     */
    public static function delete($id)
    {
        ORM::for_table(static::tableName())->findOne($id)->delete();
        return true;
    }
    
    /**
     * Return all records for the table
     * 
     * @return Array of objects
     */
    public static function getAll()
    {
        $result = ORM::for_table(static::tableName())->findMany();
        $className = static::modelName();
        $return = array();
        foreach($result as $r)
        {
            $entity = ORMUtil::mapORMToClass($className, $r, static::columnMapping());
            array_push($return, $entity);
        }
        return $return;
    }
    
    /**
     * Return a single record
     * 
     * @param int $id
     * @return Object
     */
    public static function getOne($id)
    {
        $className = static::modelName();
        $orm = ORM::for_table(static::tableName())->findOne($id);
        $obj = ORMUtil::mapORMToClass($className, $orm, static::columnMapping());
        return $obj;
    }
    
    /**
     * Saves the object
     * 
     * @param AbstractModel $entity the object
     * @return int id of saved object
     */
    public static function save(AbstractModel $entity)
    {
        $mapping = static::columnMapping();
        
        if($entity->id == 0){
            $registro = ORM::for_table(static::tableName())->create();
            $isNew = true;
        }else{
            $registro = ORM::for_table(static::tableName())->findOne($entity->id);
            $isNew = false;
        }
        
        $registro = ORMUtil::fillORMWithObject($entity, $registro, $mapping, $isNew);
        $registro->save();
        return $registro->id();

    }
 
    /**
     * Return table name
     * 
     * @throws Exception if not overwritten
     */
    protected static function tableName()
    {
        throw new Exception('Método tableName não implementado em '.static::class);
    }
    
    /**
     * Return array with column property mapping
     * @throws Exception if not overwritten
     */
    protected static function columnMapping()
    {
        throw new Exception('Método columnMapping não implementado em '.static::class);
    }
    
    protected static function modelName()
    {
        throw new Exception('Método modelName não implementado em '.static::class);
    }
    
}
