<?php

namespace LRC\filter;

/* 
 * Class that manages filters and its target lists
 */

class Filter
{
    protected $list, $filter;
    
    /*
     * initiates a filter with one filter and a target list
     * @param $filter
     * @param $list
     */
    public function __construct($filter, $list) 
    {
        $this->list = $list;
        $this->filter = $filter;   
    }
    
    /*
     * Executes the filter over the target list
     */
    public function exec()
    {
        return $this->filter->filter($this->list);
    }
}