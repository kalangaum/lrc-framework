<?php

namespace LRC\filter;

/* 
 * Interface to create new filters
 */

abstract class FilterInterface
{
    /*
     * function that iterates the filter over the list
     */
    public function filter($list) {
        return array_filter($list, $this);
    }
    
    /*
     * function that receives an element and decides if it will remain on list
     */
    public abstract function __invoke($elem);
}