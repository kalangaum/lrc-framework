<?php

namespace LRC\filter\filters;

use LRC\filter\FilterInterface;

/*
 * Filter an object by comparing a value if it is equal to one property
 */

class FilterByObjectPropertyEquals extends FilterInterface
{
    public $paramName, $paramValue;
    
    /*
     * Initiates the filter
     * @param $pName Property Name
     * @param $pValue Property Value
     */
    public function __construct($pName, $pValue)
    {
        $this->paramName = $pName;
        $this->paramValue = $pValue;
    }
    
    /*
     * Runs the comparison over an element
     */
    public function __invoke($elem)
    {;
        $pname = $this->paramName;
        return $elem->$pname === $this->paramValue;
    }
}