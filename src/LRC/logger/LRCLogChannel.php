<?php

namespace LRC\logger;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

use LRC\logger\LRCProcessorInterface;
use LRC\logger\LRCLoggerLevel;
use LRC\util\ServerUtil;

/**
 * Class that instantiates a channel
 */
class LRCLogChannel {
    
    public $minLevel, $maxLevel;
    protected $logger, $handler;
    private $toggle;
    
    /**
     * Instantiates a new channel with a name, file and min/max log levels accepted
     * 
     * @param type $name
     * @param type $filePath
     * @param type $min
     * @param type $max
     * @param type $toggle
     */
    public function __construct($name, $filePath, $min, $max, $toggle = true)
    {
        if(!file_exists($filePath))
        {
            $file = fopen($filePath, 'w') or die("can't open file");
            fclose($file);
        }
        
        $this->logger = new Logger($name);
        $this->handler = new StreamHandler($filePath);
        $this->logger->pushHandler($this->handler);
        $this->toggle = $toggle;
        $this->maxLevel = $max;
        $this->minLevel = $min;
    }
    
    /**
     * Adds a processor to the channel
     * 
     * @param \LRC\logger\LRCProcessorInterface $processor
     */
    public final function addLRCProcessor(LRCProcessorInterface $processor)
    {
        $this->logger->pushProcessor($processor);
    }
    
    /**
     * Method that logs a message into its respective channel, if the channel is toggled true
     * 
     * @param type $level
     * @param type $message
     * @param type $data
     */
    public final function log($level, $message, $data = array()) {
        
        if($this->toggle)
        {
            switch ($level)
            {
                case LRCLoggerLevel::EMERGENCY:
                    $this->logger->emergency($message, $data);
                    break;
                case LRCLoggerLevel::ALERT:
                    $this->logger->alert($message, $data);
                    break;
                case LRCLoggerLevel::CRITICAL:
                    $this->logger->critical($message, $data);
                    break;
                case LRCLoggerLevel::ERROR:
                    $this->logger->error($message, $data);
                    break;
                case LRCLoggerLevel::WARNING:
                    $this->logger->warning($message, $data);
                    break;
                case LRCLoggerLevel::NOTICE:
                    $this->logger->notice($message, $data);
                    break;
                case LRCLoggerLevel::INFO:
                    $this->logger->info($message, $data);
                    break;
                case LRCLoggerLevel::DEBUG:
                    $this->logger->debug($message, $data);
                    break;
            }
        }
        
    }
    
}
