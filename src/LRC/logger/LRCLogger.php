<?php

namespace LRC\logger;

use LRC\logger\LRCLogChannel;
use LRC\logger\LRCLoggerLevel;
use Exception;

/*
 * Manages all registered channels and sends messages through all of them
 */

class LRCLogger {
    
    private $channelList;
    
    /*
     * Instanciates a new LRCLogger
     */
    public function __construct()
    {
        $this->channelList = [];
    }
    
    /*
     * Adds a new channel
     * @param $channel
     */
    public function addChannel(LRCLogChannel $channel)
    {
        array_push($this->channelList, $channel);
    }
    
    /*
     * Send a message through all channels, if its inside the level threshold
     * @param $level
     * @param $message
     * @param $data
     * @throws Exception - if the log level is invalid
     */
    public final function log($level, $message, $data = [])
    {
        if(!LRCLoggerLevel::isValid($level))
        {
            throw new Exception('O LoggerLevel informado é invalido, por favor, use um dos tipos disponiveis na classe');
        }
        
        foreach($this->channelList as $channel)
        {
            if($level >= $channel->minLevel && $level <= $channel->maxLevel)
            {
                $channel->log($level, $message, $data);
            }
        }
       
    }
    
}
