<?php

namespace LRC\logger;

use LRC\util\EnumValidator;

/*
 * abstract class that define all log levels
 * @extends EnumValidator
 */

abstract class LRCLoggerLevel extends EnumValidator{
    
    const DEBUG = 100;
    const INFO = 200;
    const NOTICE = 250;
    const WARNING = 300;
    const ERROR = 400;
    const CRITICAL = 500;
    const ALERT = 550;
    const EMERGENCY = 600;
    
}
