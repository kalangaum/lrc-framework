<?php

namespace LRC\logger;

/*
 * Interface that defines the methods to be implemented by any processor
 */
interface LRCProcessorInterface {

    /*
     * Receives a record, and must return it with extra data inside ['extra']['key']
     * @param $record - the record before
     * @return $record -the record after, with additional data inside ['extra']
     */
    public function __invoke($record);
    
}
