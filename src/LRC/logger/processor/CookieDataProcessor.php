<?php

namespace LRC\logger\processor;

use LRC\logger\LRCProcessorInterface;
use LRC\toggles\Toggles;
use LRC\toggles\LRCToggles as T;

/*
 * Inserts $_COOKIE data
 * Works if TOGGLE_LOG_PROCESSOR_COOKIE is on
 *
 * @implements LRCProcessorInterface
 */
class CookieDataProcessor implements LRCProcessorInterface
{
    public function __invoke($record){
    
        if(Toggles::get(T::TOGGLE_LOG_PROCESSOR_COOKIE))
        {
            $record['extra']['Cookie Data: '] = $_COOKIE;
        }
        return $record;
        
    }
}