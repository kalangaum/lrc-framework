<?php

namespace LRC\logger\processor;

use LRC\logger\LRCProcessorInterface;
use LRC\toggles\Toggles;
use LRC\util\DateTimeUtil;
use LRC\toggles\LRCToggles as T;

/*
 * Inserts datetime info, based on a timezone
 * Works if TOGGLE_LOG_PROCESSOR_DATETIME is on
 *
 * @implements LRCProcessorInterface
 */
class DateTimeProcessor implements LRCProcessorInterface
{   
    
    private $tz;

    public function __construct($timezone) {
        $this->tz = $timezone;
    }
    
    public function __invoke($record){
    
        if(Toggles::get(T::TOGGLE_LOG_PROCESSOR_DATETIME))
        {
            $record['extra'][$this->tz] = DateTimeUtil::getTimeByTimeZone($this->tz);
        }
        return $record;
        
    }
}
