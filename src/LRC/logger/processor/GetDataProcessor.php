<?php

namespace LRC\logger\processor;

use LRC\logger\LRCProcessorInterface;
use LRC\toggles\Toggles;
use LRC\toggles\LRCToggles as T;

/*
 * Inserts $_GET data
 * Works if TOGGLE_LOG_PROCESSOR_GET_DATA is on and request method is GET
 *
 * @implements LRCProcessorInterface
 */
class GetDataProcessor implements LRCProcessorInterface
{
    public function __invoke($record) {
        
        if(Toggles::get(T::TOGGLE_LOG_PROCESSOR_GET_DATA) && \Flight::request()->method == 'GET')
        {            
            $record['extra']['Get Data'] = $_GET;
        }
        
        return $record;
    }
}