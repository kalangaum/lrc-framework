<?php

namespace LRC\logger\processor;

use LRC\logger\LRCProcessorInterface;
use LRC\toggles\Toggles;
use LRC\toggles\LRCToggles as T;

/*
 * Inserts logged user ID
 * Works if TOGGLE_LOG_PROCESSOR_LOGGED_USER is on
 *
 * @implements LRCProcessorInterface
 */
class LoggedUserProcessor implements LRCProcessorInterface
{
    public function __invoke($record) {
        
        if(Toggles::get(T::TOGGLE_LOG_PROCESSOR_LOGGED_USER))
        {
            $record['extra']['Logged User: '] = $_SESSION[SESSION_LOGGED_USER_ID];
        }
        
        return $record;
        
    }
}