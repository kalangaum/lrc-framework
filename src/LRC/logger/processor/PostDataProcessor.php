<?php

namespace LRC\logger\processor;

use LRC\logger\LRCProcessorInterface;
use LRC\toggles\Toggles;
use LRC\toggles\LRCToggles as T;

/*
 * Inserts $_POST data
 * Works if TOGGLE_LOG_PROCESSOR_POST_DATA is on and request method is POST
 *
 * @implements LRCProcessorInterface
 */
class PostDataProcessor implements LRCProcessorInterface
{
    public function __invoke($record) {
        
        if(Toggles::get(T::TOGGLE_LOG_PROCESSOR_POST_DATA) && \Flight::request()->method == 'POST')
        {            
            $record['extra']['Post Data'] = $_POST;
        }
        
        return $record;
    }
}