<?php

namespace LRC\logger\processor;

use LRC\logger\LRCProcessorInterface;
use LRC\toggles\Toggles;
use LRC\toggles\LRCToggles as T;

/*
 * Inserts request method, url and IP
 * Works if TOGGLE_LOG_PROCESSOR_REQUEST_DATA is on
 *
 * @implements LRCProcessorInterface
 */
class RequestDataProcessor implements LRCProcessorInterface
{
    public function __invoke($record) {
        
        if(Toggles::get(T::TOGGLE_LOG_PROCESSOR_REQUEST_DATA))
        {           
            $data = \Flight::request();
            
            $info = [];
            $info['url'] = $data->url;
            $info['method'] = $data->method;
            $info['ip'] = $data->ip;
            
            $record['extra']['Request Data'] = $info;

        }
        
        return $record;
    }
}