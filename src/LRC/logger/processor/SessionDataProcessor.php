<?php

namespace LRC\logger\processor;

use LRC\logger\LRCProcessorInterface;
use LRC\toggles\Toggles;
use LRC\toggles\LRCToggles as T;

/*
 * Inserts $_SESSION data
 * Works if TOGGLE_LOG_PROCESSOR_SESSION is on
 *
 * @implements LRCProcessorInterface
 */
class SessionDataProcessor implements LRCProcessorInterface
{
    public function __invoke($record){
    
        if(Toggles::get(T::TOGGLE_LOG_PROCESSOR_SESSION))
        {
            $record['extra']['Session Data: '] = $_SESSION;
        }
        return $record;
        
    }
}