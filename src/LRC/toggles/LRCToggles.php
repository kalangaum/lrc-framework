<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace LRC\toggles;

use LRC\util\EnumValidator;

class LRCToggles extends EnumValidator
{
    
    /*
     * TOGGLES FOR LOG PROCESSOR CLASSES
     */
    const TOGGLE_LOG_PROCESSOR_DATETIME = 'log-processor-datetime';
    const TOGGLE_LOG_PROCESSOR_SESSION =  'log-processor-session';
    const TOGGLE_LOG_PROCESSOR_COOKIE =  'log-processor-cookie';
    const TOGGLE_LOG_PROCESSOR_LOGGED_USER =  'log-processor-logged-user';
    const TOGGLE_LOG_PROCESSOR_REQUEST_DATA =  'log-processor-request-data';
    const TOGGLE_LOG_PROCESSOR_POST_DATA =  'log-processor-post-data';
    const TOGGLE_LOG_PROCESSOR_GET_DATA =  'log-processor-get-data';
    
}

