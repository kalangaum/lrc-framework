<?php

namespace LRC\toggles;

/* 
 * Interface to implement a new source for feature toggles
 */


interface ToggleConnectorInterface
{
    public function getToggleArray($src);
}
