<?php

namespace LRC\toggles;

use LRC\util\ServerUtil;
use LRC\toggles\LRCToggles;
use LRC\util\LRCConstants as C;
use Exception;


/*
 * Class that manages both system toggles and user defined feature toggles
 */

abstract class Toggles
{
    
    /*
     * get a toggle status based on a passed key
     * @param $key
     * @return bool
     */
    public static function get($key)
    {
        $data = [];
        
        if(static::isUserTogglesDefined())
        {
            $conn = static::setEnvConnector();
            $data = $conn->getToggleArray(static::getToggleDataSource());
        }
        
        if(!array_key_exists($key, $data))
        {
            if(!static::isDefaultToggle($key))
            {
                throw new Exception('Toggle inválido, por favor declare este toggle em sua fonte de toggles, ou corrija o nome do mesmo');
            }
            
            return true;
        }
        
        return $data[$key];
        
    }
    
    private static function isUserTogglesDefined()
    {
        return isset($GLOBALS[C::LRC_TOGGLES]);
    }

    private static function isDefaultToggle($key)
    {
        return LRCToggles::isValid($key);
    }
    
    private static function setEnvConnector()
    {
        $connector = $GLOBALS[C::LRC_TOGGLES][ServerUtil::getHostUrl()][C::LRC_TOGGLE_CONNECTOR];
        return new $connector();
    }
    
    private static function getToggleDataSource()
    {
        return $GLOBALS[C::LRC_TOGGLES][ServerUtil::getHostUrl()][C::LRC_TOGGLE_SOURCE];
    }
    
    
    
}
