<?php

namespace LRC\toggles\connector;

use LRC\toggles\ToggleConnectorInterface;
use LRC\util\ServerUtil;

/*
 * Class that load toggles configurator based on a Json File
 */

class JsonToggleConnector implements ToggleConnectorInterface
{
    /*
     * return an array for a given json file
     * @param $src - file
     * @return array
     */
    public function getToggleArray($src) {
        $file = file_get_contents(ServerUtil::getDocumentRoot().$src);
        return json_decode($file, true);
    }
}