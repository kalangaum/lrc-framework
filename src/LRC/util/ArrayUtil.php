<?php

namespace LRC\util;

/**
 * Class with useful functions to deals with arrays
 */
class ArrayUtil {

    /**
     * Transforms all public properties from an object into a key/value array
     * @param Objeto $object any object
     * @return Array
     */
    public static function arrayFromObject($object)
    {
        return get_object_vars($object);
    }
    
    /**
     * Maps a list of objects by its ID
     * 
     * @param type array
     * @return array mapped
     */
    public static function mapObjectListByID($list)
    {
        $return = array();
        foreach ($list as $obj)
        {
            $return[$obj->id] = $obj;
        }
        
        return $return;
    }
    
}
