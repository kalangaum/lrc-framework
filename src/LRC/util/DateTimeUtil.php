<?php

namespace LRC\util;

use DateTime;
use DateTimeZone;

/*
 * Utility class to help with dates and time
 */
class DateTimeUtil
{
    
    /*
     * Method that returns a DateTime already configured with inputted timezone
     *
     * @param $tz TimeZone to be set
     * @return DateTime
     */
    public static function getTimeByTimeZone($tz)
    {
        $datetime = new DateTime;
        $otherTZ  = new DateTimeZone($tz);
        $datetime->setTimezone($otherTZ);
        
        return $datetime;
    }
    
    /*
     * Method that return the extended weekday name in portuguese
     * @param $d - the weekday number representation
     */
    public static function getWeekDayString($d)
    {
        $semana = array(
            0 => 'Domingo',
            1 => 'Segunda',
            2 => 'Terça',
            3 => 'Quarta',
            4 => 'Quinta',
            5 => 'Sexta',
            6 => 'Sábado'
        );
        
        return $semana[$d];

    }
    
    
    /*
     * Method that returns the short month name in portuguese
     * @param $mes - month number
     */
    public static function getSiglaMes($mes)
    {
        $mesLista = array(
            1 => 'Jan',
            2 => 'Fev',
            3 => 'Mar',
            4 => 'Abr',
            5 => 'Mai',
            6 => 'Jun',
            7 => 'Jul',
            8 => 'Ago',
            9 => 'Set',
            10 => 'Out',
            11 => 'Nov',
            12 => 'Dez'
        );
        
        return $mesLista[$mes];
    }
    
}