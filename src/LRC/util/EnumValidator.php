<?php

namespace LRC\util;

/*
 * Class to make the use of enums easy
 */

use ReflectionClass;

abstract class EnumValidator {
    
    /**
     * get all avaliable constants in the enum
     * @return type array
     */
    static function getConstants()
    {
        $oClass = new ReflectionClass(static::class);
        return $oClass->getConstants();
    }
    
    /**
     * Checks if a type exists in the enum
     * @param type $type type to check
     * @return type true or false
     */
    static function isValid($type)
    {
        return in_array($type, self::getConstants());
    }
    
}
