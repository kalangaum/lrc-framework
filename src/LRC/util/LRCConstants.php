<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace LRC\util;

class LRCConstants
{
    const LRC_LOGGED_USER = 'LOGGED_USER';
    const LRC_UNLOGGED_REDIR_PAGE = 'redirPage';

    const LRC_TOGGLES = 'toggles';
    const LRC_TOGGLE_CONNECTOR = 'toggleConnector';
    const LRC_TOGGLE_SOURCE = 'toggleSource';
}

    
