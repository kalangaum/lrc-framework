<?php

namespace LRC\util;

/**
 * Class to deal with IDIORM registers and transformations to model objects
 */
class ORMUtil
{
    
    /**
     * Transforms an ORM object into its your respective model class object
     * 
     * @param String $class Class that represent the object
     * @param ORM $orm ORM object
     * @param Array $mapping column property mapping
     * @return the object from the class
     */
    public static function mapORMToClass($class, $orm, $mapping)
    {
       
        $p = new $class();
        $p->id = $orm->ID;
        
        foreach($mapping as $column => $property)
        {
            $p->$property = $orm->$column;
        }
        
        return $p;
    }
    
    /**
     * Fill up an ORM object with a given model class
     * 
     * @param Objeto $obj the model object
     * @param ORM $orm the ORM object to be filled
     * @param Array $mapping the column property mapping
     * @param isNew - if the object is a new register
     * @return ORM the ORM object filled up
     */
    public static function fillORMWithObject($obj, $orm, $mapping, $isNew = false)
    {
        
        if(!$isNew)
        {
            $orm->ID = $obj->id;
        }
        
        foreach($mapping as $column => $property)
        {
            $orm->$column = $obj->$property;
        }
        
        return $orm;
        
    }
    
}


