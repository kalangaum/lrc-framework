<?php

namespace LRC\util;

/**
 * Useful functions regarding server data
 */
class ServerUtil
{
    /**
     * Get the relative URL
     * @return url string
     */
    public static function getRelativeUrl()
    {
        $uri = $_SERVER['REQUEST_URI'];
        $query = $_SERVER['QUERY_STRING'];
        
        return str_replace('?'.$query, '', $uri);
    }
    
    public static function getHostUrl()
    {
        return $_SERVER['HTTP_HOST'];
    }
    
    public static function getDocumentRoot()
    {
        return $_SERVER['DOCUMENT_ROOT'];
    }
    
}