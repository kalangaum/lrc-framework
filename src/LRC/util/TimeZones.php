<?php

namespace LRC\util;

/* 
 * Class with useful timezone constants
 */

abstract class TimeZones
{
    const BRAZIL = 'America/Sao_Paulo';
    const CST = 'Canada/Saskatchewan';
    const CTS_DAYLIGHT_SAVING = 'CST6CDT';
    const GMT = 'GMT';
    const UCT = 'UCT';
}