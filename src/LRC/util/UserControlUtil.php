<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace LRC\util;

use LRC\util\LRCConstants as C;

/**
 * Description of UserControlUtil
 *
 * @author kalangaum
 */
class UserControlUtil
{
    public static function ifUnloggedRedirectsTo($link)
    {
        $GLOBALS[C::LRC_UNLOGGED_REDIR_PAGE] = $link;
    }
    
    public static function requireLoggedUser()
    {
        if(!isset($_SESSION[C::LRC_LOGGED_USER])){

            \Flight::redirect($GLOBALS[C::LRC_UNLOGGED_REDIR_PAGE]);
        }
    }
}
